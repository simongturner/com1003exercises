/* A simple Java program
* Written by: Simon
* First written: 29/09/2014
* Last Updated: 29/09/2014
*/

public class Exercise1A {
    
    public static void main(String[] args) {
        
        System.out.println("Hello " + args[0] + " " + args[1]);
        
    }
    
}