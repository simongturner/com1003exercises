import javax.swing.*;

public class Exercise2D{
    
    public static void main(String[] args){
        
        String firstName = JOptionPane.showInputDialog("Please enter your first name.");
        String secondName = JOptionPane.showInputDialog("Please enter your family name.");
        
        JOptionPane.showMessageDialog(null, "Your name is: " + firstName + " " + secondName, "Your name", JOptionPane.INFORMATION_MESSAGE);
 
    }
    
}