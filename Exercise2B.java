import sheffield.*;

public class Exercise2B{
    
    public static void main(String[] args){
        
        final double POUND_IN_KILOS = 0.45359237;
        final int POUNDS_IN_STONE = 14;
        
        EasyReader keyboard = new EasyReader();
        EasyWriter output = new EasyWriter();
        
        int weightInStone = keyboard.readInt("Enter a weight in stone: ");
        int weightInPounds = keyboard.readInt("Enter a weight in pounds: ");
        
        double weightInKilos = ((weightInStone * POUNDS_IN_STONE) + weightInPounds) * POUND_IN_KILOS;
        
        output.print(weightInStone + " stone and " + weightInPounds + " pounds is equivalent to ");
        output.print(weightInKilos, 2);
        output.println("kg");
        
        //System.out.println(weightInStone + " and " + weightInPounds + " is equivalent to " + weightInKilos);
        
    }
    
}