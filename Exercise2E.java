import javax.swing.*;

public class Exercise2E{
    
    public static void main(String[] args){
        
        String telNumber = JOptionPane.showInputDialog("Enter your telephone number.");
        
        int positionStartBracket = telNumber.indexOf("(");
        int positionEndBracket = telNumber.indexOf(")");
        
        String areaCode = telNumber.substring(positionStartBracket + 1, positionEndBracket);
        if(telNumber.substring(positionEndBracket + 1) == " "){
           String restOfNumber = telNumber.substring(positionEndBracket + 2);
            System.out.println("(" + areaCode + ")" + restOfNumber);
        }else{
            String restOfNumber = telNumber.substring(positionEndBracket + 1);
            System.out.println("(" + areaCode + ")" + restOfNumber);
        }
        
        
    }
    
}