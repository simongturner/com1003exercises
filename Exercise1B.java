/* A simple Java program
* Written by: Simon
* First written: 29/09/2014
* Last Updated: 29/09/2014
*/

public class Exercise1B {
    
    public static void main(String[] args){
        
        String name = "Simon";
        
        System.out.print("Hello ");
        System.out.println(name + ".");
        
    }
    
}