/* A simple Java program
* Written by: Simon
* First written: 29/09/2014
* Last Updated: 29/09/2014
*/


import sheffield.*;

public class Exercise1F {
    
    public static void main(String[] args){
        
        double p = 0.5;
        int c = 5000, n = 24;
        
        double m1 = c*p/1200*Math.pow(1+p/1200,n)/Math.pow((1+p/1200),n)-1;
        double m2 = (p/1200*Math.pow(1+p/1200,n)/Math.pow(1+p/1200,n)-1)*c;
        double m3 = c*p/1200*Math.pow(1+p/1200,n)/(Math.pow(1+p/1200,n)-1);
        double m4 = c*(p/1200*Math.pow(1+p/1200,n))/Math.pow(1+p/1200,n)-1;
        
        System.out.println(m1 + ", " + m2 + ", " + m3 + ", " + m4);
        
        
        double percent = 0.5;
        int initAmount = 5000, numMonths = 24;
        double power = Math.pow(1 + percent/1200, numMonths);
        
        double monthlyPayment = initAmount * (percent/1200 * power) / (power - 1);
        
        System.out.println("Cleaner method: " + monthlyPayment);
        
        
    }
    
}