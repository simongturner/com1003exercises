/* A simple Java program
* Written by: Simon
* First written: 29/09/2014
* Last Updated: 29/09/2014
*/

public class Exercise1E {
    
    public static void main(String[] args){
        
        int a = 4, b = 9, c = 3, d = 2;
        int result = a + b / c * d - a * c / 2;
    
        System.out.println(result);
        
    }
    
}