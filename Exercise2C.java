import sheffield.*;

public class Exercise2C{
    
    public static void main(String[] args){
        
        EasyReader fileReader = new EasyReader("data.txt");
        EasyWriter fileWriter = new EasyWriter("out.txt");
        
        final double POUNDS_IN_EUROS = 1.25;
        
        double firstValue = fileReader.readDouble();
        double secondValue = fileReader.readDouble();
        
        double firstEuroValue = firstValue * POUNDS_IN_EUROS;
        double secondEuroValue = secondValue * POUNDS_IN_EUROS;
        
        double sumValues = firstEuroValue + secondEuroValue;
        
        fileWriter.println(firstEuroValue, 2);
        fileWriter.println(secondEuroValue, 2);
        fileWriter.println(sumValues, 2);
        
        
    }
    
}